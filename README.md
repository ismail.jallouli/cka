#  Kubernetes basics: 

* Master : manage + Plan + Schedule + Monitor + Nodes
* ETCD Cluster : Our DATABASE 
* Kube-Scheduler : Howto 
* Kube-Controller-Manager 
* Kube API Server 
* Kubelet 
* Kube Proxy 

## 1. ETCD 

* **Each** informations needed, you should found on the ETCD cluster  ( Roles / accounts / secrets / configs / bindings / nodes / Nodes )
* **Any** modifications will be updated on the  ETCD Cluster

### 1.1. Installation ETCD from scratch
  
*CMD installation from scratch:* 

```
 ???? @ Houcine @ Karim
```
### 1.2. Installation ETCD from Kubeadm   

*CMD installation as POD:* 
```
 ???? @ Houcine @ Karim
```

*About the client commandLine* 
```
./etcdctl  
```
## 2. Kube-API-Server

Including all .pem to secure communications

*the view YAML api-server options - Kubeadm:*
```
cat /etc/kubernetes/manifests/kube-apiserver.yaml.   
```

*the Service api-server:*
```
cat /etc/systemd/system/kube-apiserver.service
```

*Show all kube-apiserver:*
```
ps -aux  | grep kube-apiserver
```

## 3. Kuber Controller Manager  

* @Karim could please add something about the  Health Check Status 

* What i understood -> 'Health check Status watching all time each 5 seconds  // after 40s  the pod will be putted  on unreachable status '

*The view Kube-Controller-Manager Options-kubeadm:*
```
cat /etc/kubernetes/manifests/kube-controller-manager.yaml 
```
*Option:*
```
cat /etc/systemd/system/kube-controller-manager.service 
```


## 4. Kube Scheduler

* **STEP1** Filter Nodes —> **STEP2** Rank Nodes

*the view Kube-Controller-Manager Options-kubeadm:*
```
cat /etc/kubernetes/manifests/kube-scheduler.yaml
```
*Option:*
```
cat /etc/systemd/system/kube-scheduler.service
``` 

## 5. Kubelet

* **STEP1** Register Node —>   **STEP2** Create Pods

Kubelet.service ( manually installation is required )

```
 ps -aux | grep kubelet
```


## 6. Kuber-Proxy

**Definition**  "Every pod can reach every other pod cross POD network ( internal virtual network )"

* Each POD have address IP 

* Using a service   by the name or ip 

* Service is not a container 

* the service can not join the network pod

——> that is why    ?? We have the Kube Proxy ——  ( Service —/ can communicate — Pod )
 
* Kube Proxy is the process that runs for each node
* IP TABLES  Rules For each NODE  / Micro Seg
 
-	POD network
* Service DB   identified by ip @ 



## 7. ReplicaSet

*Description* @Houcine @Karim please a description about this CMD

```
kubectl replace -f replicates-definition.ym
```

*Description* @Houcine @Karim please a description about this CMD
```
kubectl scale —replicas=6 -f replicates-definition.yml
```

*Description* @Houcine @Karim please a description about this CMD
```
kubectl scale replicas=6 replicates my app-replicaset
```


*Example of rc.yaml*
```
apiVersion: v1
kind: ReplicationController
metadata:
  name: soaktestrc
spec:
  replicas: 3
  selector:
    app: soaktestrc
  template:
    metadata:
      name: soaktestrc
      labels:
        app: soaktestrc
    spec:
      containers:
      - name: soaktestrc
        image: nickchase/soaktest
        ports:
        - containerPort: 80
```

The manifest file of replication file consist of the following fields:

* **apiVersion**: This field specifies the version of kubernetes Api to which the object belongs.
* **ReplicationController** belongs to v1 apiVersion.
* **kind**: This field specify the type of object for which the manifest belongs to.
* **metadata**: This field includes the metadata for the object. 
* **spec**: This field specifies the label selector to be used to select the pods, number of replicas of the pod to be run and the container or list of containers which the pod will run. 

*Create a Rc:*

```
kubectl create -f rc.yaml
replicationcontroller "soaktestrc" created
```
*Describe a RC created:*
```
# kubectl describe rc soaktestrc
Name:           soaktestrc
Namespace:      default
Image(s):       nickchase/soaktest
Selector:       app=soaktestrc
Labels:         app=soaktestrc
Replicas:       3 current / 3 desired
Pods Status:    3 Running / 0 Waiting / 0 Succeeded / 0 Failed
No volumes.
Events:
  FirstSeen     LastSeen        Count   From                            SubobjectPath   Type   Reason                   Message
  ---------     --------        -----   ----                            -------------   --------------                  -------
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-g5snq
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-cws05
  1m            1m              1       {replication-controller } 
```

```
# kubectl get pods
NAME               READY     STATUS    RESTARTS   AGE
soaktestrc-cws05   1/1       Running   0          3m
soaktestrc-g5snq   1/1       Running   0          3m
soaktestrc-ro2bl   1/1       Running   0          3m
```
```
# kubectl delete rc soaktestrc
replicationcontroller "soaktestrc" deleted

# kubectl get pods
```
*Example of rs.yml:*
```
apiVersion: apps/v1
kind: ReplicaSet
metadata: 
  name: nginx-rs
spec:
  replicas: 3
  selector:
    matchLabels:
      env: prod
    matchExpressions:
    - { key: tier, operator: In, values: [frontend] }
  template:
    metadata:
      name: nginx
      labels: 
        env: prod
        tier: frontend
    spec:
      containers:
      - name: nginx-container
        image: nginx
        ports:
        - containerPort: 80
```

* **matchLabel**: works exactly same as the equality based selector,
* **matchExpression**: used to specify the set based selectors.
* **apiVersion**:  replicaSet is apps/v1. 
* **Rest all is same as the replication controller.**

*create a rs:*
```
kubectl create -f nginx-rs.yml
```
*Get the ReplicaSet:*
```
kubectl get rs/nginx-rs
kubectl get rs/nginx-rs -o wide
kubectl get rs/nginx-rs -o yaml
kubectl get rs/nginx-rs -o json
```
*Describe the ReplicaSet:*
```
kubectl describe rs/nginx-rs
```

*To scale up the replicas:*
```
kubectl scale rs nginx-rs --replicas=5
```

*To delete the ReplicaSet:*
```
1. kubectl delete rs nginx-rs
2. kubectl delete -f nginx-rs.yml 
```
### 7.1. The main **difference** between a Replica Set and a Replication Controller

#### 7.1.a. Is the selector support.
```
+--------------------------------------------------+-----------------------------------------------------+
|                   Replica Set                    |               Replication Controller                |
+--------------------------------------------------+-----------------------------------------------------+
| Replica Set supports the new set-based selector. | Replication Controller only supports equality-based |
| This gives more flexibility. for eg:             | selector. for eg:                                   |
|          environment in (production, qa)         |             environment = production                |
|  This selects all resources with key equal to    | This selects all resources with key equal to        |
|  environment and value equal to production or qa | environment and value equal to production           |
+--------------------------------------------------+-----------------------------------------------------+
```
#### 7.1.b. The second thing is the updating the pods.

```
+-------------------------------------------------------+-----------------------------------------------+
|                      Replica Set                      |            Replication Controller             |
+-------------------------------------------------------+-----------------------------------------------+
| rollout command is used for updating the replica set. | rolling-update command is used for updating   |
| Even though replica set can be used independently,    | the replication controller. This replaces the |
| it is best used along with deployments which          | specified replication controller with a new   |
| makes them declarative.                               | replication controller by updating one pod    |
|                                                       | at a time to use the new PodTemplate.         |
+-------------------------------------------------------+-----------------------------------------------+
```

## 8. Usually  CMD 



*comment to add*
```
kubectl cluster-info
```
*comment to add*
```
kubectl get nodes
```

*comment to add*
```
kubectl get pods
```

*comment to add*
```
kubectl describe pods
```

*comment to add*
```
kubectl get services
```

*command to add*
```
kubectl expose deployement/kubernetes-bootcamp --type "NodePort" --port 8080
kubectl get services
kubectl describe service/kubernetes-bootcamp
```

*Enabling pods access*
```
kubectl proxy
```


*comment to add*
```
kubectl exec 
```

*comment to add*
```
kubectl logs
```

```
Apiversion: v1
Kind: Pod
Metadata:
     name: nginx
Spec: 
  Containers:
Image: ngnix
name : ngnix 
```


```
kubectl expose pod redis --port=6379 --name redis-service
```

I need your help ++